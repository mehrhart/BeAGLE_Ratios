//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Jan 31 16:42:14 2018 by ROOT version 5.34/36
// from TChain BeAGLEevents/
//////////////////////////////////////////////////////////

#ifndef Ratios_h
#define Ratios_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <iostream>

// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.

class Ratios {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Int_t           ievent;
   Double_t        truey;
   Double_t        trueQ2;
   Double_t        truex;
   Double_t        trueW2;
   Double_t        trueNu;
   Double_t        b;
   Double_t        Thickness;
   Double_t        ThickScl;
   Int_t           Nnevap;
   Int_t           Npevap;
   Int_t           Aremn;
   Double_t        d1st;
   Double_t        Eexc;
   Int_t           nrTracksFS;
   Int_t           I[442];   //[nrTracksFS]
   Int_t           ISTHKK[442];   //[nrTracksFS]
   Int_t           IDHKK[442];   //[nrTracksFS]
   Int_t        JMOHKK1[442];   //[nrTracksFS]
   Int_t        JMOHKK2[442];   //[nrTracksFS]
   Int_t        JDAHKK1[442];   //[nrTracksFS]
   Int_t        JDAHKK2[442];   //[nrTracksFS]
   Double_t        PHKK1[442];   //[nrTracksFS]
   Double_t        PHKK2[442];   //[nrTracksFS]
   Double_t        PHKK3[442];   //[nrTracksFS]
   Double_t        PHKK4[442];   //[nrTracksFS]
   Double_t        PHKK5[442];   //[nrTracksFS]
   Double_t        VHKK1[442];   //[nrTracksFS]
   Double_t        VHKK2[442];   //[nrTracksFS]
   Double_t        VHKK3[442];   //[nrTracksFS]
   Int_t           IDRES[442];   //[nrTracksFS]
   Int_t           IDXRES[442];   //[nrTracksFS]
   Int_t           NOBAM[442];   //[nrTracksFS]

   // List of branches
   TBranch        *b_ievent;   //!
   TBranch        *b_truey;   //!
   TBranch        *b_trueQ2;   //!
   TBranch        *b_truex;   //!
   TBranch        *b_trueW2;   //!
   TBranch        *b_trueNu;   //!
   TBranch        *b_b;   //!
   TBranch        *b_Thickness;   //!
   TBranch        *b_ThickScl;   //!
   TBranch        *b_Nnevap;   //!
   TBranch        *b_Npevap;   //!
   TBranch        *b_Aremn;   //!
   TBranch        *b_d1st;   //!
   TBranch        *b_Eexc;   //!
   TBranch        *b_nrTracksFS;   //!
   TBranch        *b_I;   //!
   TBranch        *b_ISTHKK;   //!
   TBranch        *b_IDHKK;   //!
   TBranch        *b_JMOHKK1;   //!
   TBranch        *b_JMOHKK2;   //!
   TBranch        *b_JDAHKK1;   //!
   TBranch        *b_JDAHKK2;   //!
   TBranch        *b_PHKK1;   //!
   TBranch        *b_PHKK2;   //!
   TBranch        *b_PHKK3;   //!
   TBranch        *b_PHKK4;   //!
   TBranch        *b_PHKK5;   //!
   TBranch        *b_VHKK1;   //!
   TBranch        *b_VHKK2;   //!
   TBranch        *b_VHKK3;   //!
   TBranch        *b_IDRES;   //!
   TBranch        *b_IDXRES;   //!
   TBranch        *b_NOBAM;   //!

   Ratios(TTree *tree=0);
   virtual ~Ratios();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);

};

#endif

#ifdef Ratios_cxx
Ratios::Ratios(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {

#ifdef SINGLE_TREE
      // The following code should be used if you want this class to access
      // a single tree instead of a chain
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Memory Directory");
      if (!f || !f->IsOpen()) {
         f = new TFile("Memory Directory");
      }
      f->GetObject("BeAGLEevents",tree);

#else // SINGLE_TREE

      // The following code should be used if you want this class to access a chain
      // of trees.
      TChain * chain = new TChain("BeAGLEevents","");
      chain->Add("../data/raw/clean/ePb_06_11_0435_clean.root/BeAGLEevents");
      //chain->Add("../data/raw/clean/ePb_06_10_0725_clean.root/BeAGLEevents");
      //chain->Add("../data/raw/clean/ePb_06_11_0435_clean.root/BeAGLEevents");
      tree = chain;
#endif // SINGLE_TREE

   }
   Init(tree);
}

Ratios::~Ratios()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t Ratios::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
//   char newDatafile[100] = fChain->GetCurrentFile()->GetName();
//   std::cout << "file name : " << newDatafile << endl;
   return fChain->GetEntry(entry);
}
Long64_t Ratios::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void Ratios::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("ievent", &ievent, &b_ievent);
   fChain->SetBranchAddress("truey", &truey, &b_truey);
   fChain->SetBranchAddress("trueQ2", &trueQ2, &b_trueQ2);
   fChain->SetBranchAddress("truex", &truex, &b_truex);
   fChain->SetBranchAddress("trueW2", &trueW2, &b_trueW2);
   fChain->SetBranchAddress("trueNu", &trueNu, &b_trueNu);
   fChain->SetBranchAddress("b", &b, &b_b);
   fChain->SetBranchAddress("Thickness", &Thickness, &b_Thickness);
   fChain->SetBranchAddress("ThickScl", &ThickScl, &b_ThickScl);
   fChain->SetBranchAddress("Nnevap", &Nnevap, &b_Nnevap);
   fChain->SetBranchAddress("Npevap", &Npevap, &b_Npevap);
   fChain->SetBranchAddress("Aremn", &Aremn, &b_Aremn);
   fChain->SetBranchAddress("d1st", &d1st, &b_d1st);
   fChain->SetBranchAddress("Eexc", &Eexc, &b_Eexc);
   fChain->SetBranchAddress("nrTracksFS", &nrTracksFS, &b_nrTracksFS);
   fChain->SetBranchAddress("I", I, &b_I);
   fChain->SetBranchAddress("ISTHKK", ISTHKK, &b_ISTHKK);
   fChain->SetBranchAddress("IDHKK", IDHKK, &b_IDHKK);
   fChain->SetBranchAddress("JMOHKK1", JMOHKK1, &b_JMOHKK1);
   fChain->SetBranchAddress("JMOHKK2", JMOHKK2, &b_JMOHKK2);
   fChain->SetBranchAddress("JDAHKK1", JDAHKK1, &b_JDAHKK1);
   fChain->SetBranchAddress("JDAHKK2", JDAHKK2, &b_JDAHKK2);
   fChain->SetBranchAddress("PHKK1", PHKK1, &b_PHKK1);
   fChain->SetBranchAddress("PHKK2", PHKK2, &b_PHKK2);
   fChain->SetBranchAddress("PHKK3", PHKK3, &b_PHKK3);
   fChain->SetBranchAddress("PHKK4", PHKK4, &b_PHKK4);
   fChain->SetBranchAddress("PHKK5", PHKK5, &b_PHKK5);
   fChain->SetBranchAddress("VHKK1", VHKK1, &b_VHKK1);
   fChain->SetBranchAddress("VHKK2", VHKK2, &b_VHKK2);
   fChain->SetBranchAddress("VHKK3", VHKK3, &b_VHKK3);
   fChain->SetBranchAddress("IDRES", IDRES, &b_IDRES);
   fChain->SetBranchAddress("IDXRES", IDXRES, &b_IDXRES);
   fChain->SetBranchAddress("NOBAM", NOBAM, &b_NOBAM);
   Notify();
}

Bool_t Ratios::Notify()
{

   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void Ratios::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry, 500);
}
Int_t Ratios::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef Ratios_cxx
